import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AddProductComponent} from "./core/components/add-product/add-product.component";
import {EditComponent} from "./core/components/edit/edit.component";
import {ProductsComponent} from "./core/components/products/products.component";
import {LoginComponent} from "./core/components/user-account/login/login.component";
import {RegisterComponent} from "./core/components/user-account/register/register.component";
import {ResetPasswordComponent} from "./core/components/user-account/reset-password/reset-password.component";
import {AuthGuard} from "./core/resources/services/auth.guard";


const routes: Routes = [] = [
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'reset-password', component: ResetPasswordComponent},
    {path: 'add-product', component: AddProductComponent, canActivate:[AuthGuard]},
    {path: 'edit-product/:id',component: EditComponent},
    {path: '', component: ProductsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes), RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

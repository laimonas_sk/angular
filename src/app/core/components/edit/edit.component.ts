import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../products/products.service";
import {ActivatedRoute} from "@angular/router";
import {Router} from "@angular/router";
import {FormGroup, FormControl, Validators, FormBuilder} from "@angular/forms";
import {AuthorisationService} from "../../resources/services/authorisation.service";
import {ProductData} from "../../resources/models/user.model";

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
    rocketId: any = this.route.snapshot.paramMap.get('id');
    product: ProductData;
    isLoggedIn = this.authService.isLoggedIn;

    editProductForm = new FormGroup({
        productTitle: new FormControl(),
        productImage: new FormControl(''),
        productPrice: new FormControl('')
    })

    constructor(private productsService : ProductsService, private route: ActivatedRoute, private router: Router, private formBuilder: FormBuilder, private authService: AuthorisationService) {
    }

    ngOnInit(): void {
        this.editProductForm = this.formBuilder.group({
            productTitle: [
                '',
                Validators.required
            ],
            productImage: [
                '',
                Validators.required
            ],
            productPrice: [
                '',
                [
                    Validators.required,
                    Validators.min(1)
                ]
            ]
        });

        this.productsService.getProduct(this.rocketId).subscribe(product => {
            this.product = product;
            this.editProductForm.patchValue({
                productTitle: product.title,
                productImage: product.image,
                productPrice: product.price
            })
        })
    }

    editProduct() {
        const obj = {
            title: this.editProductForm.value.productTitle,
            image: this.editProductForm.value.productImage,
            price: this.editProductForm.value.productPrice,
            id: this.rocketId
        }

        this.productsService.editProduct(this.rocketId, obj).subscribe(() => {
            alert('Rocket "' + this.editProductForm.value.productTitle + '" has been added successfully!');
            this.router.navigate(['/']);
        });
    }

    deleteProduct() {
        this.productsService.deleteProduct(this.rocketId).subscribe(
            () => {
                alert('Rocket has been deleted!');
                    this.router.navigate(['']);
            }
        )
    }

    get editProductsFormControl() {
        return this.editProductForm.controls;
    }
}

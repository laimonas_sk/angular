import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../products/products.service";
import {Router} from "@angular/router";
import {FormGroup, FormBuilder, Validators, AsyncValidatorFn} from "@angular/forms";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {ProductData} from "../../resources/models/user.model";

@Component({
    selector: 'app-add-product',
    templateUrl: './add-product.component.html',
    styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
    addProductForm: FormGroup;

    products: Observable<ProductData[]> = this.productsService.getProducts();

    constructor(private productsService: ProductsService, private router: Router, private formBuilder: FormBuilder) {
    }

    ngOnInit(): void {

        this.addProductForm = this.formBuilder.group({
            productTitle: [
                '',
                [Validators.required],
                [this.uniqueProductTitleValidator()]
            ],
            productImage: [
                '',
                Validators.required
            ],
            productPrice: [
                '',
                [
                    Validators.required,
                    Validators.min(1)
                ]
            ]
        });
    }

    createProduct() {

        let inputId = Date.now();

        const obj = {
            title: this.addProductForm.value.productTitle,
            image: this.addProductForm.value.productImage,
            price: this.addProductForm.value.productPrice,
            id: inputId
        }
        this.productsService.addProduct(obj).subscribe(() => {
            alert('Rocket "' + this.addProductForm.value.productTitle + '" has been added successfully!');
            this.router.navigate(['/']);
        });
    }

    resetForm() {
        this.addProductForm.reset();
    }

    uniqueProductTitleValidator(): AsyncValidatorFn {
        return (control) => {
            const title = control.value;
            return this.productsService.getProducts().pipe(map(products => {
                const existingTitle = products.some(product => product.title === title);
                if (existingTitle) {
                    return {
                        uniqueTitle: 'Title exists'
                    }
                } else {
                    return null;
                }
            }))
        }
    }

    get addProductsFormControl() {
        return this.addProductForm.controls;
    }
}

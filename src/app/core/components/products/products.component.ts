import {Component, OnInit} from '@angular/core';
import {ProductsService} from "./products.service";
import {AuthorisationService} from "../../resources/services/authorisation.service";
import {Filter, ProductData} from "../../resources/models/user.model";

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {

    filters: Filter[] = [
        {
            id: 1,
            amount: 100,
            equality: 'less'
        },
        {
            id: 2,
            amount: 500,
            equality: 'more'
        }
    ];

    selectedFilterIds: number[] = [];

    isLoggedIn = this.authService.isLoggedIn;

    products: ProductData[];

    constructor(private productsService: ProductsService, private authService: AuthorisationService) {
    }

    ngOnInit(): void {
         this.productsService.getProducts().subscribe({
            next: result => {
                console.log(result);
                this.products = result;
            },
            error: err => {
                console.log(err);
            },
            complete: () => {
                console.log('complete');
            }
        });
    }

    addToCartClick(product: ProductData) {
        this.productsService.addToCart(product).subscribe(() => {
        this.productsService.getCartProducts();
        });
    }

    onFilterOn(filter: Filter) {
        this.selectedFilterIds.push(filter.id);
    }

    onFilterOff(filter: Filter) {
        this.selectedFilterIds = this.selectedFilterIds.filter(id => filter.id != id)
    }
}

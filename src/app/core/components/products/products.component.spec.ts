import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ProductsComponent} from './products.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FiltersComponent} from "../../resources/filters/filters.component";
import {FilterPricePipe} from "../../resources/filters/filter-price.pipe";

describe('ProductsComponent', () => {
    let component: ProductsComponent;
    let fixture: ComponentFixture<ProductsComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ProductsComponent, FiltersComponent, FilterPricePipe],
            imports: [HttpClientTestingModule]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ProductsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

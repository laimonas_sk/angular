import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {BehaviorSubject} from "rxjs";
import {ProductData} from "../../resources/models/user.model";

@Injectable({
    providedIn: 'root'
})

export class ProductsService {

    db = 'http://localhost:3000/products';
    dbCart = 'http://localhost:3000/cart';

    private cartObserver = new BehaviorSubject<ProductData[]>([]);
    public sharedCart = this.cartObserver.asObservable();

    constructor(private http: HttpClient) {
    }

    getProducts(): Observable<ProductData[]> {
        return this.http.get<ProductData[]>(this.db);
    }

    getCartProducts(): void {
        this.http.get<ProductData[]>(this.dbCart).subscribe((x: ProductData[]) => {
                this.cartObserver.next(x);
            }
        )
    }

    getProduct(id: string): Observable<ProductData> {
        return this.http.get<ProductData>(`${this.db}/${id}`);
    }

    addToCart(product: ProductData): Observable<void> {
        product.id = Date.now();
        return this.http.post<void>(this.dbCart, product);
    }

    addProduct(product: ProductData): Observable<void> {
        return this.http.post<void>(this.db, product);
    }

    editProduct(id: string, product: ProductData) : Observable<ProductData> {
        return this.http.put<ProductData>(`${this.db}/${id}`, product);
    }

    deleteProduct(id: string): Observable<void> {
        return this.http.delete<void>(`${this.db}/${id}`);
    }

    deleteCartProduct(id: number): Observable<void> {
        return this.http.delete<void>(`${this.dbCart}/${id}`);
    }

    searchProducts(keyword: string): Observable<any> {
        return this.http.get(`${this.db}?title_like=${keyword}`)
    }
}

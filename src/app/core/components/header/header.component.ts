import {Component, HostListener, OnInit} from '@angular/core';
import {ProductsService} from "../products/products.service";
import {ElementRef} from "@angular/core";
import {LoginComponent} from "../user-account/login/login.component";
import {Subscription} from "rxjs";
import {AuthorisationService} from "../../resources/services/authorisation.service";
import {ProductData} from "../../resources/models/user.model";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
    searchKeyword: string;
    result: ProductData[];

    userFromSession: string;
    subscription: Subscription;

    constructor(private productsService: ProductsService, private eRef: ElementRef, private loginComp: LoginComponent, private authService: AuthorisationService) {
    }

    onSearchClick(keyword: string) {
        this.productsService.searchProducts(keyword).subscribe((res) => {
           res ? this.result = res : 'nothing found';
        });
    }

    public isSearchOpen: boolean = true;

    @HostListener('document:click', ['$event'])
    closeSearchWindow(event: any) {
        this.isSearchOpen = !!this.eRef.nativeElement.contains(event.target);
    }

    ngOnInit(): void {
        this.subscription = this.authService.userFromSession.subscribe(user => this.userFromSession = user);
    }

    logOutClick() {
        this.loginComp.logOut();
    }
}

import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators, AsyncValidatorFn} from "@angular/forms";
import {AuthorisationService} from "../../../resources/services/authorisation.service";
import {map} from "rxjs/operators";
import {Router} from "@angular/router";

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;

    constructor(private formBuilder: FormBuilder, private authService: AuthorisationService, private router: Router) {
    }

    ngOnInit(): void {
        this.registerForm = this.formBuilder.group({
            userName: [
                '',
                [Validators.required],
                [this.existingUserValidator()]
            ],
            userPassword: [
                '',
                [Validators.required]
            ]
        })
    }

    get registerFormControl() {
        return this.registerForm.controls;
    }

    existingUserValidator(): AsyncValidatorFn {
        return (control) => {
            const userName = control.value;
            return this.authService.getAllUsers().pipe(map(users => {
                const existingUserName = users.some((user: { userName: string; }) => user.userName === userName);
                if (existingUserName) {
                    return {
                        uniqueUserName: 'Username exists'
                    }
                } else {
                    return null;
                }
            }))
        }
    }

    createUser() {

        let userId = Date.now();

        const user = {
            id: userId,
            userName: this.registerForm.value.userName,
            userPassword: this.registerForm.value.userPassword,
        }
        this.authService.createNewUser(user).subscribe(() => {
            alert('User "' + this.registerForm.value.userName + '" has been created');
            alert(this.registerForm.value.userName + '", please login with your credentials');
            this.router.navigate(['login']);
        })
    }

    resetForm() {
        this.registerForm.reset();
    }

}

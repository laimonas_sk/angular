import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators, AsyncValidatorFn} from "@angular/forms";
import {AuthorisationService} from "../../../resources/services/authorisation.service";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    userFromSession: string;
    subscription: Subscription;


    constructor(private formBuilder: FormBuilder, private http: HttpClient, private authService: AuthorisationService, private router: Router) {
    }

    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            userName: [
                '',
                [Validators.required]
            ],
            userPassword: [
                '',
                [Validators.required]
            ]
        });

        this.subscription = this.authService.userFromSession.subscribe(user => {
            this.userFromSession = user;
            console.log("user from session ", user);
        });
    }


    get loginFormControl() {
        return this.loginForm.controls;
    }

    onClickLoginUser() {
        this.authService.loginUser(this.loginFormControl.userName.value, this.loginFormControl.userPassword.value).subscribe({
            next: (x: any) => {
                // on success
                console.log(x);
                this.router.navigate(['/']);
            }, error: (x: any) => {
                // on fail
                console.log(x);
                alert('No user was found');
            }
        });
    }

    logOut() {
        this.authService.isLoggedIn = false;
        sessionStorage.clear();
        this.authService.getUserFromSession('');
    }

    resetForm() {
        this.loginForm.reset();
    }
}

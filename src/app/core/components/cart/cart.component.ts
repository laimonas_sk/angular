import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../products/products.service";
import {ProductData} from "../../resources/models/user.model";

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

    showCart : boolean = false;
    cartProducts: ProductData[];

    constructor(private productsService: ProductsService) {
    }

    onCartClick() {
        this.showCart = !this.showCart;
    }

    ngOnInit(): void {
        this.productsService.getCartProducts();
        this.productsService.sharedCart.subscribe(cartData => this.cartProducts = cartData)
    }
    
    removeFromCart(item: ProductData) {

        let id = item.id;
        console.log('item ID: ' + id);

        this.productsService.deleteCartProduct(id).subscribe(
            () => {
                this.productsService.getCartProducts();
            }
        )
    }
}

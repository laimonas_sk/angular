export interface UserType {
    "id": number,
    "userName": string,
    "userPassword": string
}

export interface ProductData {
    title: string,
    image: string,
    price: number,
    id: number,
}

export interface Filter {
    id: number;
    amount: number;
    equality: 'less' | 'more'
}
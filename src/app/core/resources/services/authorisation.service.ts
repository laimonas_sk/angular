import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {UserType} from "../models/user.model";

@Injectable({
    providedIn: 'root'
})

export class AuthorisationService {
    users = 'http://localhost:3000/users';
    session = 'http://localhost:3000/session';

    loggedInUser: any = false;
    private loggedInUserSource: BehaviorSubject<string> = this.loggedInUser;
    userFromSession;
    isLoggedIn: boolean;

    constructor(private http: HttpClient) {
        this.loggedInUserSource = new BehaviorSubject(sessionStorage.getItem('loggedInUser') || '');
        this.userFromSession = this.loggedInUserSource.asObservable();
    }

    getUserFromSession(user: string) {
        this.loggedInUserSource.next(user);
    }

    getAllUsers(): Observable<UserType[]> {
        return this.http.get<UserType[]>(this.users);
    }

    createNewUser(user: object): Observable<UserType> {
        return this.http.post<UserType>(this.users, user);
    }

    loginUser(userName: string, password: string): Observable<UserType[]> {
        return this.getAllUsers().pipe(tap(res => {
            const user = res.find((user: UserType) => {
                return user.userName === userName && user.userPassword === password;
            });
            if (user) {
                this.isLoggedIn = true;
                alert("Login successful");
                sessionStorage.setItem('loggedInUser', user.userName);
                sessionStorage.setItem('isLoggedIn', JSON.stringify(this.isLoggedIn));

                this.getUserFromSession(user.userName);
                return user;

            } else {
                alert('No user was found');
                this.isLoggedIn = false;
                return false;
            }
        }));
    }
}

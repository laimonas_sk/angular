import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthorisationService} from "./authorisation.service";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthorisationService, private router: Router) {
    }
    canActivate() {
        if (this.authService.isLoggedIn) {
            return true;
        } else {
            this.router.navigate(['login']);
            return false;
        }
    }

}

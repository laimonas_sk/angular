import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable, throwError} from "rxjs";
import {catchError, finalize, retry} from "rxjs/operators";

@Injectable()
export class HttpErrorInterceptorService implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        req = req.clone();
        return next.handle(req).pipe(
            retry(2),
            catchError((err: HttpErrorResponse) => {
                alert(`HTTP Error: ${req.url}`);
                return throwError(err);
            }),
            finalize(() => {
                const profilingMessage = `Request method: ${req.method} "${req.urlWithParams}"`;
                console.log(profilingMessage);
            })
        );
    }
}

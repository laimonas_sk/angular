import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';
import {Filter} from "../models/user.model";

@Component({
    selector: 'app-filters',
    templateUrl: './filters.component.html',
    styleUrls: ['./filters.component.scss']
})

export class FiltersComponent implements OnInit {

    @Input() filters: Filter[];
    @Input() selectedIds: number[];
    @Output() filterOn: EventEmitter<Filter> = new EventEmitter<Filter>();
    @Output() filterOff: EventEmitter<Filter> = new EventEmitter<Filter>();

    constructor() {
    }

    isSelected(filter: Filter): boolean {
        return this.selectedIds.some(id => id === filter.id);
    }

    onClick(filter: Filter) {
        const selected = this.isSelected(filter);
        if (selected) {
            this.filterOff.emit(filter);
        } else {
            this.filterOn.emit(filter);
        }
    }

    ngOnInit(): void {
    }
}

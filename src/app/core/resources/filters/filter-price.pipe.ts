import {Pipe, PipeTransform} from '@angular/core';
import {filter} from "rxjs/operators";
import {Filter, ProductData} from "../models/user.model";

@Pipe({
    name: 'filterPrice',
    pure: false
})
export class FilterPricePipe implements PipeTransform {

    transform(allProducts: ProductData[], filters: Filter[], selectedIds: number[]): ProductData[] {
        const selectedFilters = filters.filter(filter => selectedIds.includes(filter.id));
        if (selectedFilters.length === 0) {
            return allProducts;
        }
        return allProducts.filter((product) => {
            return selectedFilters.some(filter => this.isFiltered(product, filter));
        });
    }

    private isFiltered = (product: ProductData, filter: Filter) => filter.equality === 'less' ? product.price <= filter.amount : product.price >= filter.amount;
}

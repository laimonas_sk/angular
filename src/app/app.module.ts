import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './core/components/header/header.component';
import {FooterComponent} from './core/components/footer/footer.component';
import {AddProductComponent} from './core/components/add-product/add-product.component';
import {EditComponent} from './core/components/edit/edit.component';
import {ProductsComponent} from './core/components/products/products.component';
import { CartComponent } from './core/components/cart/cart.component';
import { FilterPricePipe } from './core/resources/filters/filter-price.pipe';
import { FiltersComponent } from './core/resources/filters/filters.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {HttpErrorInterceptorService} from "./core/resources/services/httperrorinterceptor.service";
import {LoginComponent} from "./core/components/user-account/login/login.component";
import {RegisterComponent} from "./core/components/user-account/register/register.component";


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        AddProductComponent,
        EditComponent,
        ProductsComponent,
        CartComponent,
        FilterPricePipe,
        FiltersComponent,
        RegisterComponent,
        LoginComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        HttpClientModule,
        FormsModule,
        // AuthModule
    ],
    providers: [CartComponent, FilterPricePipe, FiltersComponent, LoginComponent,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpErrorInterceptorService,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

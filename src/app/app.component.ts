import {Component, OnInit} from '@angular/core';
import {AuthorisationService} from "./core/resources/services/authorisation.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'angular-workshop';

    constructor(private authService: AuthorisationService) {
    }

    ngOnInit(): void {
        this.authService.isLoggedIn = JSON.parse(<string>sessionStorage.getItem('isLoggedIn'));
    }
}
